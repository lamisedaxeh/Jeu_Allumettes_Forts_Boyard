#include <iostream>
#include<time.h>

using namespace std;
const int  N = 20;

void saisie(char nom[20],char *niv,int *nb_allumettes,char *first){
    int taille;

    cout << "Saisie du nom du joueur : ";
    cin >> nom;

    do{
        cout << "Saisie contrôlée du niveau de l'ordinateur (niveau 'n' pour \"naif\" ou 'e' pour \"expert\") : ";
        cin >> *niv;
    }while(*niv != 'n' && *niv != 'e');

    do{
        cout << "taille du jeu [0<=20] : ";
        cin >> *nb_allumettes;
    }while(*nb_allumettes > N && *nb_allumettes > 0);

    do{
        cout << "Saisie contrôlée du premier joueur ('j' pour utilisateur ou 'o' pour ordinateur) : ";
        cin >> *first;
    }while(*first != 'j' && *first != 'o');
}

 void Affiche(int nb_allumettes){
    int x = 0;
    for(int i = 0 ; i < nb_allumettes ; i++){
        cout << 'I';
        x++;
        if(x == 5){
            cout << endl;
            x = 0 ;
        }
    }
    cout << endl; 
 }

int jeuordi(char niv,int nb_allumettes){
    int nb_temps = 0;
    cout << "BOT LA MARMOTTE joue !" << endl;
    if (niv == 'n'){
       nb_temps = rand()%3+1;//												NAIF // aleatoire
    }else{
       do{//																Expert inteligent
           nb_temps = nb_temps + 1 ;
       }while(((nb_allumettes-nb_temps)-1)%4 != 0 && nb_allumettes >= 3);// compte la distance avant le prochain multiple de 4+1
       if(nb_allumettes <= 3 ){
           nb_temps = nb_allumettes-1;// OWNED LE joueur si le noombre d'allumettes < 3
       }
       if(nb_temps > 3 ){
           nb_temps = 1;//													si la distance avant le prochain multiple de 4+1 > 3 alors patiente le temps que le joueur face une erreur 
       }
       /*nb_temps = nb_allumettes/4-1;
        if(nb_temps > 3){
            nb_temps = 3;
        }else if(nb_temps < 1){
            nb_temps = 1;
        }*/
    }
    cout << "BOT LA MARMOTTE pioche !" << nb_temps << endl;
    return nb_temps;
}

int jeujoueur(char nom[20]){
    int nb_temps = 0;
    cout << nom << " a toi de jouer !" << endl;
    char abandon;
    
    do{
        cout << nom << " combien tu prend d'allumettes ? [0:abandon / 1 / 2 / 3 ] : ";//		Algo jeu joueur 
        cin >> nb_temps;
        if(nb_temps < 0 || nb_temps > 3){
            cout << "[erreur] Tu dois pioché entre 0 abandon ou 1 2 3 allumettes !" << endl;
        }else if(nb_temps == 0){
            cout << "Veux tu vraiment abandoné comme un lache ? [y/n] : ";
            cin >> abandon;
            if(abandon == 'n'){
                nb_temps =  -12000;
            }
        }
        
    }while(nb_temps < 0 || nb_temps > 3);
    return nb_temps;
}

void jeualtern(char *tour,char niv,int *nb_allumettes, char nom[20]){// Gestion du joueur
    int temps = 0;
    if ( *tour == 'o' ){
        temps = jeuordi(niv,*nb_allumettes);
        *tour = 'j';
    }else{
        cout << "La pluie continue de tombée !" << endl;
        temps = jeujoueur(nom);
        if (temps == 0)// 											Abandon
            temps = *nb_allumettes;
        *tour = 'o';
    }

    *nb_allumettes = *nb_allumettes - temps; // 					Suppresion des allumettes
    
}


int main()
{
    srand(time(NULL));
    char nom[20];
    char niv;
    int nb_allumettes = 0;
    char tour;
    saisie(nom,&niv,&nb_allumettes,&tour);
    //cout << nom << niv << nb_allumettes << tour << endl;
    
    cout << "C'est l'heure du DUDUDUDUDUDUDUDUDUUUUUUEL" << endl;// 			Introduction 
    
    cout << "Il commence a pleuvoir !" << endl;
    
    do{// 																		Boucle de jeu
        Affiche(nb_allumettes);
        jeualtern(&tour,niv,&nb_allumettes,nom);
    }while(nb_allumettes > 0);
    
    if (tour == 'o'){// 														resultat
        cout << "BOT LA MARMOTTE a gagner sans trop de mal !" << endl;
        cout << nom << " a perdu contre un BOT LA MARMOTTE !" << endl;
    }else{
        cout << nom << " a gagner avec peine !" << endl;
        cout << "BOT LA MARMOTTE vous a laissez gagner !" << endl;
    }
    
    //cout << nom << niv << nb_allumettes << tour << endl;
	
	/*
	*													                                                                                                                                                                                                                                                                 
	*                                                                                            .---.                    _______                                                                                                                                     
	*                  .--.                   _________   _...._                                 |   |                    \  ___ `'.                 __.....__               __  __   ___                            __.....__               __  __   ___             
	*     _.._         |__|                   \        |.'      '-.                              |   |                     ' |--.\  \            .-''         '.            |  |/  `.'   `.                      .-''         '.            |  |/  `.'   `.           
	*   .' .._|        .--.     .|             \        .'```'.    '.          .-,.--.           |   |                     | |    \  ' .-,.--.  /     .-''"'-.  `.          |   .-.  .-.   '               .|   /     .-''"'-.  `.          |   .-.  .-.   '          
	*   | '       __   |  |   .' |_             \      |       \     \   __    |  .-. |          |   |    __               | |     |  '|  .-. |/     /________\   \    __   |  |  |  |  |  |             .' |_ /     /________\   \    __   |  |  |  |  |  |          
	* __| |__  .:--.'. |  | .'     |             |     |        |    |.:--.'.  | |  | |          |   | .:--.'.             | |     |  || |  | ||                  | .:--.'. |  |  |  |  |  |           .'     ||                  | .:--.'. |  |  |  |  |  |          
	*|__   __|/ |   \ ||  |'--.  .-'             |      \      /    ./ |   \ | | |  | |          |   |/ |   \ |            | |     ' .'| |  | |\    .-------------'/ |   \ ||  |  |  |  |  |          '--.  .-'\    .-------------'/ |   \ ||  |  |  |  |  |          
	*   | |   `" __ | ||  |   |  |               |     |\`'-.-'   .' `" __ | | | |  '-           |   |`" __ | |            | |___.' /' | |  '-  \    '-.____...---.`" __ | ||  |  |  |  |  |             |  |   \    '-.____...---.`" __ | ||  |  |  |  |  |          
	*   | |    .'.''| ||__|   |  |               |     | '-....-'`    .'.''| | | |               |   | .'.''| |           /_______.'/  | |       `.             .'  .'.''| ||__|  |__|  |__|             |  |    `.             .'  .'.''| ||__|  |__|  |__|          
	*   | |   / /   | |_      |  '.'            .'     '.            / /   | |_| |               '---'/ /   | |_          \_______|/   | |         `''-...... -'   / /   | |_                            |  '.'    `''-...... -'   / /   | |_                         
	*   | |   \ \._,\ '/      |   /           '-----------'          \ \._,\ '/|_|                    \ \._,\ '/                       |_|                         \ \._,\ '/                            |   /                     \ \._,\ '/                         
	*   |_|    `--'  `"       `'-'                                    `--'  `"                         `--'  `"                                                     `--'  `"                             `'-'                       `--'  `"                          
	*													
	*													*     ,MMM8&&&.            *
	*														  MMMM88&&&&&    .
	*														 MMMM88&&&&&&&
	*											 *           MMM88&&&&&&&&
	*														 MMM88&&&&&&&&
	*														 'MMM88&&&&&&'
	*														   'MMM8&&&'      *    
	*												  |\___/|     /\___/\
	*												  )     (     )    ~( .              '
	*												 =\     /=   =\~    /=
	*												   )===(       ) ~ (
	*												  /     \     /     \
	*												  |     |     ) ~   (
	*												 /       \   /     ~ \
	*												 \       /   \~     ~/
	*										  jgs_/\_/\__  _/_/\_/\__~__/_/\_/\_/\_/\_/\_
	*										  |  |  |  |( (  |  |  | ))  |  |  |  |  |  |
	*										  |  |  |  | ) ) |  |  |//|  |  |  |  |  |  |
	*										  |  |  |  |(_(  |  |  (( |  |  |  |  |  |  |
	*										  |  |  |  |  |  |  |  |\)|  |  |  |  |  |  |
	*										  |  |  |  |  |  |  |  |  |  |  |  |  |  |  |
	*
	*/
    return 0;
}
